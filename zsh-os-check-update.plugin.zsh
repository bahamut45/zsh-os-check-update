if which tput >/dev/null 2>&1; then
    ncolors=$(tput colors)
fi

if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    BLUE="$(tput setaf 4)"
    NORMAL="$(tput sgr0)"
else
    BLUE=""
    BOLD=""
    NORMAL=""
fi

zmodload zsh/datetime

function command_exists() {
    command -v "$@" >/dev/null 2>&1
}

function _current_epoch() {
    echo $(( $EPOCHSECONDS / 60 / 60 / 24 ))
}

function _update_zsh_custom_os_check_update() {
    echo "LAST_EPOCH=$(_current_epoch)" >| "${ZSH_CACHE_DIR}/.zsh-custom-os-check-update"
}

function os_check_update() {
    if command_exists /usr/lib/update-notifier/apt-check; then
        DISTRIB="$(/usr/bin/lsb_release -ds)"
        printf "${BLUE}%s${NORMAL}\n" "Check ${DISTRIB} update"
        sudo apt update
        /usr/lib/update-notifier/apt-check --human-readable
    else
        printf "${RED}%s${NORMAL}\n" "This package 'update-notifier-common' is not installed. Please install 'update-notifier-common' first."
    fi
    if [ $(uname -r | sed -n 's/.*\( *Microsoft *\).*/\1/ip') ]; then
        printf "${BLUE}%s${NORMAL}\n" "Check WSL update"
        /usr/bin/do-release-upgrade -c
    fi
}

epoch_target=${OS_CHECK_UPDATE:-13}

if [ -f "${ZSH_CACHE_DIR}/.zsh-custom-os-check-update" ]; then
    source "${ZSH_CACHE_DIR}/.zsh-custom-os-check-update"

    LAST_EPOCH=${LAST_EPOCH:-0}

    epoch_diff=$(($(_current_epoch) - $LAST_EPOCH))
    if [ $epoch_diff -gt $epoch_target ]; then
        if [ "$DISABLE_UPDATE_PROMPT" = "true" ]; then
            (os_check_update)
        else
            echo "[OS update] Would you like to check updates? [Y/n]: \c"
            read line
            if [[ "$line" == Y* ]] || [[ "$line" == y* ]] || [ -z "$line" ]; then
                (os_check_update)
            fi
        fi
        _update_zsh_custom_os_check_update
    fi
else
    _update_zsh_custom_os_check_update
fi

unset -f _update_zsh_custom_os_check_update _current_epoch