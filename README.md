# zsh-os-check-update

[oh-my-zsh plugin](https://github.com/robbyrussell/oh-my-zsh) for check update with `Debian`/`Ubuntu` and `WSL`.

## Demo

![demo](https://i.imgur.com/wVQ5Efa.png)

## Install

Install `update-notifier-common`.

Create a new directory in `$ZSH_CUSTOM/plugins` called `zsh-os-check-update` and clone this repo into that directory.

```shell
apt install update-notifier-common # Required for human readable update.

git clone https://gitlab.com/bahamut45/zsh-os-check-update $ZSH_CUSTOM/plugins/zsh-os-check-update
```

## Usage

Add `zsh-os-check-update` to the `plugin=()` list in your `~/.zshrc` file and you're done.

The check updates will be executed automatically as soon as 13 days by default.

If you want to check for updates more often, you can adjust this line in the `~/.zshrc` file.
Default command:
```shell
# Uncomment the following line to change how often to check update (in days).
# export OS_CHECK_UPDATE=13
```
Changed command: (checks daily for check update)
```shell
# Uncomment the following line to change how often to check update (in days).
export OS_CHECK_UPDATE=1
```

## Credits

Inspired by [https://github.com/TamCore/autoupdate-oh-my-zsh-plugins](https://github.com/TamCore/autoupdate-oh-my-zsh-plugins).
